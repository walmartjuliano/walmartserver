/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author julia_000
 */
/*
 * To change this license header, choose License HeaderesultSet in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Juliano
 */
public class DataBaseAccess {

    private static final int NO_RESULT_SET_PROPERTY = -1;

    public final static int CURRENT_VALUE = 1;

    public final static int NEXT_VALUE = 2;

    private Connection conn;

    private ResultSet resultSet;

    private Statement statement;

    private PreparedStatement preparedStatement;

    public DataBaseAccess() throws ClassNotFoundException, SQLException {
        conn = null;
    }

    public void Commit() throws SQLException {
        if (conn != null) {
            conn.commit();
        }
        closeResultSet();
        closeStatement();
    }

    public void Rollback() throws SQLException {
        if (conn != null) {
            conn.rollback();
        }
        closeResultSet();
        closeStatement();
    }

    public void InitAcess() throws ClassNotFoundException, SQLException {

        String driver = "org.apache.derby.jdbc.ClientDriver";
        String dbName = "MalhaWalmart";
        String connectionURL = "jdbc:derby://localhost:1527/" + dbName + ";create=true;user=walmart;password=walmart";

        Class.forName(driver);

        if (conn == null) {
            conn = DriverManager.getConnection(connectionURL);
        }
    }

    public void closeResultSet() throws SQLException {
        if (resultSet != null) {
            resultSet.close();
        }
    }

    public void closeConnection() throws SQLException {
        if (conn != null) {
            conn.close();
        }
    }

    public void closeStatement() throws SQLException {
        if (statement != null) {
            statement.close();
        }
    }

    public void closeAllResources() throws SQLException {
        resultSet.close();
        statement.close();
        conn.close();
    }

    public ResultSet Select(String[] columns, String[] where, String[] orderby) throws SQLException {
        return Select(columns, where, orderby, NO_RESULT_SET_PROPERTY, NO_RESULT_SET_PROPERTY, NO_RESULT_SET_PROPERTY);
    }

    public ResultSet Select(String[] columns, String[] where, String[] orderby, int ResultSetType, int ResultSetConcurrency) throws SQLException {
        return Select(columns, where, orderby, ResultSetType, ResultSetConcurrency, NO_RESULT_SET_PROPERTY);
    }

    public ResultSet Select(String[] columns, String[] where, String[] orderby, int ResultSetType) throws SQLException {
        return Select(columns, where, orderby, ResultSetType, NO_RESULT_SET_PROPERTY, NO_RESULT_SET_PROPERTY);
    }

    private ResultSet Select(String[] columns, String[] where, String[] orderby, int ResultSetType, int ResultSetConcurrency, int ResultSetHoldability) throws SQLException {
        String Table = "MALHA";
        String Columns = "";
        String Where = "";
        String SelectStatement = "";
        String OrderBy = "";

        for (String column : columns) {
            Columns += column + ",";
        }
        // Remove last comma
        Columns = Columns.substring(0, Columns.length() - 1);

        if (!Columns.isEmpty()) {
            SelectStatement = "SELECT " + Columns + " FROM " + Table;

            for (String where1 : where) {
                Where += where1 + " AND ";
            }

            Where = Where.substring(0, Where.length() - " AND ".length());
            if (!Where.isEmpty()) {
                SelectStatement += " WHERE " + Where;
            }

            for (String orderBy1 : orderby) {
                OrderBy += orderBy1 + ",";
            }
            OrderBy = OrderBy.substring(0, OrderBy.length() - 1);
            if (!OrderBy.isEmpty()) {
                SelectStatement += " ORDER BY " + OrderBy;
            }
        }
        if (conn.isValid(0)) {

            if (ResultSetType == NO_RESULT_SET_PROPERTY && ResultSetConcurrency == NO_RESULT_SET_PROPERTY && ResultSetHoldability == NO_RESULT_SET_PROPERTY) {
                statement = conn.createStatement();
            } else if (ResultSetType != NO_RESULT_SET_PROPERTY && ResultSetConcurrency != NO_RESULT_SET_PROPERTY && ResultSetHoldability != NO_RESULT_SET_PROPERTY) {
                statement = conn.createStatement(ResultSetType, ResultSetConcurrency, ResultSetHoldability);
            } else if (ResultSetType != NO_RESULT_SET_PROPERTY && ResultSetConcurrency != NO_RESULT_SET_PROPERTY && ResultSetHoldability == NO_RESULT_SET_PROPERTY) {
                statement = conn.createStatement(ResultSetType, ResultSetConcurrency);
            }
            resultSet = statement.executeQuery(SelectStatement);
        } else {
            resultSet = null;
        }

        return resultSet;
    }

    public void InsertInto(Malha m) throws SQLException {
        String Table = "MALHA";
        String columns = "";
        String InsertStatement = "";

        columns = "NOME, DESTINO, DISTANCIA, ORIGEM";

        if (!Table.isEmpty() && !columns.isEmpty()) {

            InsertStatement = "INSERT INTO " + Table + "(" + columns + ")" + " VALUES " + "(";

            InsertStatement += "'" + m.getNome() + "','" + m.getDestino() + "'," + m.getDistancia().toString() + ",'" + m.getOrigem() + "'";

            InsertStatement += ")";
        }

        ExecuteStatement(InsertStatement);
    }

    public void Update(String[] set, String[] where) throws SQLException {
        String Table = "";
        String Where = "";
        String Set = "";
        String UpdateStatement = "";

        for (String set1 : set) {
            Set += set1 + ",";
        }
        Set = Set.substring(0, Set.length() - 1);

        for (String where1 : where) {
            Where += where1 + ",";
        }
        Where = Where.substring(0, Where.length() - 1);

        if (!Table.isEmpty() && !Set.isEmpty() && !Where.isEmpty()) {

            UpdateStatement = "UPDATE " + Table + " SET " + Set + " WHERE " + Where;

            statement = conn.createStatement();
            statement.executeUpdate(UpdateStatement);
        }
    }

    public int ExecuteStatement(String Statement) throws SQLException {
        int ret = 0;

        if (!Statement.isEmpty()) {
            statement = conn.createStatement();
            ret = statement.executeUpdate(Statement);
        }

        return ret;
    }

    public int ExecutePreparedStatement(String statement, Object[] args) throws SQLException {
        int ret = 0;

        int numArgs = args.length;
        int count = 0;

        preparedStatement = conn.prepareStatement(statement);

        while (numArgs >= 1) {
            if (args[count].getClass() == Integer.class) {
                preparedStatement.setInt(count
                        + 1, (int) args[count]);
            }

            if (args[count].getClass() == String.class) {
                preparedStatement.setString(count
                        + 1, (String) args[count]);
            }

            if (args[count].getClass() == Boolean.class) {
                preparedStatement.setBoolean(count
                        + 1, (boolean) args[count]);
            }

            if (args[count].getClass() == Date.class) {
                preparedStatement.setDate(count
                        + 1, (Date) args[count]);
            }

            count++;
            numArgs--;
        }

        ret = preparedStatement.executeUpdate();

        return ret;
    }

    public ResultSet ExecuteQuery(String query) {
        try {
            statement = conn.createStatement();
            resultSet = statement.executeQuery(query);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "The database returns the following error: \n" + ex, "Database Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(DataBaseAccess.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return resultSet;
    }

    public void Delete(String[] where) throws SQLException {
        String Table = "";
        String Where = "";
        String DeleteStatement = "";

        for (String where1 : where) {
            Where += where1 + " AND ";
        }
        Where = Where.substring(0, Where.length() - " AND ".length());

        if (!Table.isEmpty() && !Where.isEmpty()) {

            DeleteStatement = "DELETE FROM " + Table + " WHERE " + Where;

            statement = conn.createStatement();
            statement.executeUpdate(DeleteStatement);
        }

    }

    public class Caminho {

        private String Caminho;
        private Float Distancia;

        public Caminho() {
            Caminho = "";
            Distancia = 0f;
        }

        public String getCaminho() {
            return Caminho;
        }

        public void setCaminho(String Caminho) {
            this.Caminho = Caminho;
        }

        public Float getDistancia() {
            return Distancia;
        }

        public void setDistancia(Float Distancia) {
            this.Distancia = Distancia;
        }

        public void AddDistancia(Float Distancia) {
            this.Distancia += Distancia;
        }

        public void AddCaminho(String Caminho) {
            if (!this.Caminho.contains(Caminho)) {
                this.Caminho += " " + Caminho;
            }
        }

        public void add(Caminho c) {
            AddCaminho(c.getCaminho());
            AddDistancia(c.getDistancia());
        }

    }

    public Caminho CalcM(String Nome, String Origem, String Destino) throws SQLException {
        Caminho c = new Caminho();

        ResultSet rs = EncontrarDestino(Nome, Origem, Destino);
        c.AddCaminho(Origem);

        if (!rs.next()) {
            rs = EncontrarDestino(Nome, Origem);
            if (!rs.next()) {
                return c;
            } else {
                rs.beforeFirst();
                while (rs.next()) {
                    c.AddCaminho(rs.getString("DESTINO"));
                    ResultSet rs2 = EncontrarDestino(Nome, rs.getString("DESTINO"), Destino);
                    if (rs2.next() && rs2.getString("DESTINO").equals(Destino)) {
                        c.AddDistancia(rs2.getFloat("DISTANCIA"));
                        c.add(CalcM(Nome, Origem, rs.getString("DESTINO")));
                        break;
                    }
                }
            }
        } else {
            c.AddDistancia(rs.getFloat("DISTANCIA"));
        }
        
        c.AddCaminho(Destino);
        return c;
    }

    public ResultSet EncontrarDestino(String Nome, String Origem, String Destino) throws SQLException {

        ResultSet r = Select(new String[]{"NOME, DESTINO, DISTANCIA, ORIGEM"},
                new String[]{"NOME = " + "'" + Nome + "'", "ORIGEM = " + "'" + Origem + "'", "DESTINO = " + "'" + Destino + "'"},
                new String[]{"DISTANCIA"}, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

        return r;
    }

    public ResultSet EncontrarDestino(String Nome, String Origem) throws SQLException {

        ResultSet r = Select(new String[]{"NOME, DESTINO, DISTANCIA, ORIGEM"},
                new String[]{"NOME = " + "'" + Nome + "'", "ORIGEM = " + "'" + Origem + "'"},
                new String[]{"DISTANCIA"}, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

        return r;
    }

}
