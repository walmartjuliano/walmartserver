/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.io.Serializable;

/**
 *
 * @author julia_000
 */
public class Malha implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Nome;
    private String Origem;
    private String Destino;
    private Float Distancia;

    public Malha(String Nome, String Origem, String Destino, Float Distancia) {
        this.Nome = Nome;
        this.Origem = Origem;
        this.Destino = Destino;
        this.Distancia = Distancia;
    }

    public Malha() {
        this.Nome = "";
        this.Origem = "";
        this.Destino = "";
        this.Distancia = 0f;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (Nome != null ? Nome.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Malha)) {
            return false;
        }
        Malha other = (Malha) object;
        return !((this.Nome == null && other.Nome != null) || (this.Nome != null && !this.Nome.equals(other.Nome)));
    }

    @Override
    public String toString() {
        return "DAO.Malha[ id=" + Nome + " ]";
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getOrigem() {
        return Origem;
    }

    public void setOrigem(String Origem) {
        this.Origem = Origem;
    }

    public String getDestino() {
        return Destino;
    }

    public void setDestino(String Destino) {
        this.Destino = Destino;
    }

    public Float getDistancia() {
        return Distancia;
    }

    public void setDistancia(Float Distancia) {
        this.Distancia = Distancia;
    }

}
