/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import DAO.DataBaseAccess;
import DAO.DataBaseAccess.Caminho;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author julia_000
 */
@WebService(serviceName = "CalcularMalha")
public class CalcularMalha {

    /**
     * Web service operation
     *
     * @param Nome
     * @param Origem
     * @param Destino
     * @param Autonomia
     * @param PrecoLitro
     * @return
     */
    @WebMethod(operationName = "Calcular")
    public String Calcular(@WebParam(name = "Nome") String Nome, @WebParam(name = "Origem") String Origem, @WebParam(name = "Destino") String Destino, @WebParam(name = "Autonomia") Float Autonomia, @WebParam(name = "PrecoLitro") Float PrecoLitro) {
        //TODO write your implementation code here:

        DataBaseAccess dao = null;

        try {
            dao = new DataBaseAccess();
            dao.InitAcess();

            Caminho c = dao.CalcM(Nome, Origem, Destino);

            return Nome + " " + c.getCaminho() + " " + String.valueOf(PrecoLitro * (c.getDistancia() / Autonomia));
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(InserirMalha.class.getName()).log(Level.SEVERE, null, ex);
            return "Erro no banco: " + ex.getMessage();
        }

    }

}
