/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import DAO.DataBaseAccess;
import DAO.Malha;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author julia_000
 */
@WebService(serviceName = "InserirMalha")
public class InserirMalha {
    /**
     * This is a sample web service operation
     */
    /**
     * Web service operation
     *
     * @param Nome
     * @param Origem
     * @param Destino
     * @param Distancia
     * @return
     */
    @WebMethod(operationName = "InserirMalha")
    public String InserirMalha(@WebParam(name = "Nome") String Nome, @WebParam(name = "Origem") String Origem, @WebParam(name = "Destino") String Destino, @WebParam(name = "Distancia") float Distancia) {
        //TODO write your implementation code here:
        

        // Create new malha
        Malha malha = new Malha(Nome, Origem, Destino, Distancia);
        
         DataBaseAccess dao = null;
         
        try {
           dao = new DataBaseAccess();
           dao.InitAcess();
           dao.InsertInto(malha);
            return "Gravei";
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(InserirMalha.class.getName()).log(Level.SEVERE, null, ex);
             return "Erro no banco: " + ex.getMessage();
        }
        
        

       
    }

    /**
     * This is a sample web service operation
     */
}
